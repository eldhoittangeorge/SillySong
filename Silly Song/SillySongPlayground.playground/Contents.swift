//: Playground - noun: a place where people can play

import UIKit




// join an array of strings into a single template string:
let bananaFanaTemplate = [
    "<FULL_NAME>, <FULL_NAME>, Bo B<SHORT_NAME>",
    "Banana Fana Fo F<SHORT_NAME>",
    "Me My Mo M<SHORT_NAME>",
    "<FULL_NAME>"].joinWithSeparator("\n")




func shortNameFromName(fullName: String) -> String {
    
    let lowercaseName = fullName.lowercaseString
    let vowelSet  = NSCharacterSet(charactersInString: "aeiou")
    
    if let firstVowelRange = lowercaseName.rangeOfCharacterFromSet(vowelSet, options: .CaseInsensitiveSearch) {
        return lowercaseName.substringFromIndex(firstVowelRange.startIndex)
    }
    
    return lowercaseName
    
}

shortNameFromName("Bob")
shortNameFromName("bob")
shortNameFromName("Emily")
shortNameFromName("emily")
shortNameFromName("Jorge")
shortNameFromName("jorge")
shortNameFromName("Ana")
shortNameFromName("ana")




func lyricsForName(lyricsTemplate: String, fullName: String) -> String {
    
    let shortName = shortNameFromName(fullName)
    
    let lyrics = lyricsTemplate
    .stringByReplacingOccurrencesOfString("<FULL_NAME>", withString: fullName)
    .stringByReplacingOccurrencesOfString("<SHORT_NAME>", withString: shortName)
    
//    let full =  lyrics.stringByReplacingOccurrencesOfString("<FULL_NAME>", withString: fullName)
//    let shortAndFull = full.stringByReplacingOccurrencesOfString("<SHORT_NAME>", withString: shortName)
//    
//    return shortAndFull
    
    return lyrics
    
}


var myLyrics = lyricsForName(bananaFanaTemplate, fullName: "Jorge")






